package com.test;

import java.util.Scanner;

public class CountCharacterDigit {
public static void main(String[] args) {
	String s1 = "Hello123Welcome0";
	int count = 0; 
	int digit = 0; 
	for (int i = 0; i < s1.length(); i++) { 
		char ch = s1.charAt(i);
		if (Character.isAlphabetic(ch)) { 
			count++; 
		} else {
			digit++;
		}
	}
	System.out.println("Characters : " + count );
	System.out.println("Digits : " + digit);
}
}

