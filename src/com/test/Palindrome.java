package com.test;

import java.util.Scanner;

public class Palindrome {
public static void main(String[] args) {
	Scanner s = new Scanner(System.in);
	System.out.println("Enter the Number");
	int num = s.nextInt(), 
	temp = num;
	int reverse = 0;
	while (num > 0) {
		int reminder = num % 10;
		reverse = reverse * 10 + reminder;
		num = num / 10;
	}
	if (temp == reverse) {
		System.out.println("It is palinderome");
	} else {
		System.out.println("It is not a palindrome");
	}
}
}

